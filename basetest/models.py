from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Userprofile(models.Model):
    user=models.OneToOneField(User)
    mobile=models.CharField(max_length=15,null=True)

    class Meta:
        verbose_name="用户资料"
        verbose_name_plural="用户资料"
