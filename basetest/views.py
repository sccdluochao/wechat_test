import json

import urllib3
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
#登陆
from django.views.decorators.csrf import csrf_exempt

from basetest.models import Userprofile
from wechattest import settings


#@csrf_exempt
def login(request):
    rs={"status":0,"msg":""}

    #current_datetime = str(datetime.datetime.now())
    #dev_key = "55a7042d338dff78107d7b7599e26ba6"
    """
    headers = {
        "x-app-key": "c45d5465",
        "x-request-date": current_datetime,
        "x-session-key": hashlib.md5((current_datetime + dev_key).encode('utf-8')).hexdigest(),
        # 身份证正面
        "x-task-config": "capkey=ocr.cloud.template,property=idcard,templateIndex=0,templatePageIndex=0",
        # 身份证反面
        # "x-task-config": "capkey=ocr.cloud.template,property=idcard,templateIndex=0,templatePageIndex=1",
        "x-sdk-version": "5.2"
    }
    """
    http = urllib3.PoolManager()
    urllib3.disable_warnings()
    try:
        resp = http.request('GET',
                         "https://api.weixin.qq.com/sns/jscode2session?appid="\
                         +settings.AppID+"&secret="+settings.AppSecret\
                         +"&js_code="+request.GET['code']\
                         +"&grant_type=authorization_code")
                         #body=buffer.getvalue(),
                         #headers=headers\
    except Exception:
        rs['msg'] = "出现了一个问题，请稍后再试，或检查网络是否通畅！"
        return JsonResponse(rs)
    r_msg = json.loads(resp.data.decode('utf-8'))

    # 请求成功
    if resp.status == 200 and "errcode" in r_msg:
        rs["msg"] = r_msg['errmsg']
    elif resp.status == 200:
        request.session['openid'] = r_msg['openid']
        request.session['session_key'] = r_msg['session_key']
        users=User.objects.filter(username=r_msg['openid'])
        rs['status'] = 1

        #没有用户信息创建用户信息
        if users.count()<=0:
            user = User.objects.create_user(r_msg['openid'], r_msg['openid'] + "@wechat.com", r_msg['openid'])
            user.save()
            up=Userprofile.objects.create(user=user)
            up.save()
            rs['status']=2
        else:
            #判断用户是否有电话
            up=Userprofile.objects.filter(user=users[0])
            if up[0].mobile == "":
                rs['status'] = 2

        user = authenticate(request, username=r_msg['openid'], password=r_msg['openid'])
        if user is not None:
            login(request, user)
        else:
            rs['status'] = 0
    else:
        rs["msg"] = "登陆错误！"

    return JsonResponse(rs)
