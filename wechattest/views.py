from django.http import JsonResponse

def page_not_found(request):
    rs={"status":404}
    return JsonResponse(rs)


def page_error(request):
    rs = {"status": 500}
    return JsonResponse(rs)


def permission_denied(request):
    rs = {"status": 403}
    return JsonResponse(rs)
